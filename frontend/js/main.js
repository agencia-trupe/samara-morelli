(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.mobileButton = {
        toggleState: function(event) {
            event.preventDefault();

            var $handle = $(this),
                $nav    = $('#nav-mobile');

            $nav.slideToggle();
            $handle.toggleClass('close');
        },

        init: function() {
            $('#mobile-toggle').on('click touchstart', this.toggleState);
        }
    };

    App.viewport = function() {
        var e = window, a = 'inner';
        if (!('innerWidth' in window )) {
            a = 'client';
            e = document.documentElement || document.body;
        }
        return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
    };

    App.isMobile = function(mobileWidth) {
        mobileWidth = mobileWidth || 1040;
        return (this.viewport().width < mobileWidth);
    };

    App.bannersHome = function () {
        var $wrapper = $('#banners-home');
        if (!$wrapper.length) return;

        $wrapper.cycle({
            slides: '>div'
        });
    };

    App.projetosNav = {
        init: function() {
            var $navegacao = $('.projetos-info');
            if (!$navegacao.length) return;

            this.posicionaNav($navegacao);

            var _this = this;
            $(window).on('resize scroll orientationchange load', function() {
                if (!App.isMobile()) {
                    _this.posicionaNav($navegacao);
                } else {
                    $navegacao.removeClass('stick').attr('style', '');
                }
            });
        },

        posicionaNav: function(obj) {
            var windowPos = $(window).scrollTop(),
                navPos    = $(document).outerHeight() - obj.outerHeight() - 207;

            if (windowPos >= 147 && navPos > windowPos) {
                obj.attr('style', '').addClass('stick');
            } else {
                obj.removeClass('stick');
                if (windowPos >= navPos) {
                    obj.css({'position':'absolute', 'bottom':'30px'});
                }
            }
        }
    };

    App.clippingNav = {
        init: function() {
            var $navegacao = $('.clipping-navegacao');
            if (!$navegacao.length) return;

            this.posicionaNav($navegacao);
            this.mostraNav($navegacao);

            var _this = this;
            $(window).on('resize scroll orientationchange load', function() {
                _this.posicionaNav($navegacao);
                _this.mostraNav($navegacao);
            });
        },

        mostraNav: function(obj) {
            if (App.isMobile(1280)) {
                obj.show();
            } else if ($(window).scrollTop() > 170 || App.viewport().height > 400) {
                obj.fadeIn();
            } else {
                obj.fadeOut();
            }
        },

        posicionaNav: function(obj) {
            var windowPos    = $(window).scrollTop(),
                scrollBottom = $(document).height() - $(window).height() - $(window).scrollTop(),
                bottomHeight = $('footer').height() + 135;

            if (App.isMobile(1280)) {
                obj.removeAttr('style');
                return false;
            }

            obj.css('right', ((parseInt($(window).width()) - 980) / 2) - 115);

            if (scrollBottom <= bottomHeight - 35) {
                obj.css({
                    'position' : 'absolute',
                    'right'    : '-115px'
                });
            } else {
                obj.css({
                    'position' : 'fixed',
                    'bottom'   : '33px'
                });
            }
        }
    };

    App.contatoForm = {
        envio: function(event) {
            event.preventDefault();

            var $formContato  = $(this),
                $formSuccesso = $('#sucesso'),
                $formErro     = $('#erro');


            $formErro.fadeOut();

            $.post(BASE + '/contato', {

                nome     : $('#nome').val(),
                email    : $('#email').val(),
                telefone : $('#telefone').val(),
                mensagem : $('#mensagem').val()

            }, function(data) {

                if (data.status == 'success') $formContato.fadeOut('slow', function() {
                    $formSuccesso.fadeIn('slow');
                    return;
                });

                $formErro.fadeIn('slow');

            }, 'json');
        },

        init: function() {
            var $form = $('#form-contato');
            if (!$form.length) return;

            $form.on('submit', this.envio);
        }
    };

    App.init = function() {
        this.mobileButton.init();
        this.bannersHome();
        this.projetosNav.init();
        this.clippingNav.init();
        this.contatoForm.init();
    };

    $(document).ready(function() {
        App.init();
    });

}(window, document, jQuery));
