-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 21-Jul-2015 às 15:25
-- Versão do servidor: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `samaramorelli`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `banners`
--

INSERT INTO `banners` (`id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 0, '20150716150254_DSC00063.JPG', '2015-07-16 18:02:57', '2015-07-16 18:02:57'),
(2, 2, '20150716150302_IMG_3531.JPG', '2015-07-16 18:03:03', '2015-07-16 18:03:03'),
(3, 1, '20150716150309_DSC00047.JPG', '2015-07-16 18:03:12', '2015-07-16 18:03:12');

-- --------------------------------------------------------

--
-- Estrutura da tabela `clipping`
--

CREATE TABLE IF NOT EXISTS `clipping` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `clipping_slug_unique` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Extraindo dados da tabela `clipping`
--

INSERT INTO `clipping` (`id`, `slug`, `data`, `titulo`, `capa`, `created_at`, `updated_at`) VALUES
(6, 'lorem-ipsum-dolor-sit-amet', '2015-01', 'Lorem ipsum dolor sit amet', '20150720161319_02.jpg', '2015-07-20 19:13:20', '2015-07-20 19:24:20'),
(8, 'lorem-ipsum-dolor-sit-amet-2', '2015-02', 'Lorem ipsum dolor sit amet 2', '20150720161552_03.jpg', '2015-07-20 19:15:54', '2015-07-20 19:24:27'),
(9, 'lorem-ipsum-dolor-sit-amet-3', '2015-07', 'Lorem ipsum dolor sit amet 3', '20150720161604_04.jpg', '2015-07-20 19:16:07', '2015-07-20 19:16:07'),
(10, 'lorem-ipsum-dolor-sit-amet-4', '2015-07', 'Lorem ipsum dolor sit amet 4', '20150720161616_05.jpg', '2015-07-20 19:16:18', '2015-07-20 19:16:18'),
(11, 'lorem-ipsum-dolor-sit-amet-5', '2015-07', 'Lorem ipsum dolor sit amet 5', '20150720161643_05.jpg', '2015-07-20 19:16:45', '2015-07-20 19:16:45'),
(12, 'lorem-ipsum-dolor-sit-amet-6', '2016-01', 'Lorem ipsum dolor sit amet 6', '20150720161745_17.jpg', '2015-07-20 19:16:56', '2015-07-20 19:24:37');

-- --------------------------------------------------------

--
-- Estrutura da tabela `clipping_imagens`
--

CREATE TABLE IF NOT EXISTS `clipping_imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `clipping_id` int(11) NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Extraindo dados da tabela `clipping_imagens`
--

INSERT INTO `clipping_imagens` (`id`, `clipping_id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 4, 0, '20150716162418_02.jpg', '2015-07-16 19:24:19', '2015-07-16 19:24:19'),
(2, 4, 0, '20150716162425_03.jpg', '2015-07-16 19:24:27', '2015-07-16 19:24:27'),
(3, 4, 0, '20150716162431_IMG_3520.JPG', '2015-07-16 19:24:33', '2015-07-16 19:24:33'),
(4, 4, 0, '20150716163538_IMG_3520.JPG', '2015-07-16 19:35:39', '2015-07-16 19:35:39'),
(5, 12, 0, '20150720161810_02.jpg', '2015-07-20 19:18:11', '2015-07-20 19:18:11'),
(6, 12, 0, '20150720161816_04.jpg', '2015-07-20 19:18:18', '2015-07-20 19:18:18'),
(7, 12, 0, '20150720161822_17.jpg', '2015-07-20 19:18:24', '2015-07-20 19:18:24'),
(8, 12, 0, '20150720161828_20.jpg', '2015-07-20 19:18:30', '2015-07-20 19:18:30');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE IF NOT EXISTS `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ddd` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `googlemaps` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `email`, `ddd`, `telefone`, `endereco`, `googlemaps`, `facebook`, `instagram`, `created_at`, `updated_at`) VALUES
(1, 'samara@samaramorelli.com.br', '19', '98431.0403', '<p>Rua Baronesa Geraldo de Resende, 42/133</p>\r\n\r\n<p>Jardim Nossa Senhora Auxiliadora</p>\r\n\r\n<p>13075-270 . Campinas/SP</p>\r\n', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3675.7587711970123!2d-47.056326300000045!3d-22.885367899999995!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94c8c5fe67aa741f%3A0xb11d89d998ad6305!2sCondom%C3%ADnio+Edif%C3%ADcio+St+Marcel+-+R.+Baroneza+Geraldo+de+Resende%2C+42+-+Vila+Rossi+e+Borchi%2C+Campinas+-+SP%2C+13076-060!5e0!3m2!1spt-BR!2sbr!4v1437072369352" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', 'http://www.facebook.com/', 'http://www.instagram.com/', '0000-00-00 00:00:00', '2015-07-16 21:50:04');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_recebidos`
--

CREATE TABLE IF NOT EXISTS `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Extraindo dados da tabela `contatos_recebidos`
--

INSERT INTO `contatos_recebidos` (`id`, `nome`, `email`, `telefone`, `mensagem`, `lido`, `created_at`, `updated_at`) VALUES
(7, 'q', 'a@a.com', '', 'Q', 0, '2015-07-20 21:11:36', '2015-07-20 21:11:36');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_01_28_130645_create_usuarios_table', 1),
('2015_07_15_140834_create_banners_table', 1),
('2015_07_15_140847_create_perfil_table', 1),
('2015_07_15_140915_create_projetos_table', 1),
('2015_07_15_140924_create_projetos_categorias_table', 1),
('2015_07_15_140932_create_projetos_imagens_table', 1),
('2015_07_15_140943_create_clipping_table', 1),
('2015_07_15_140954_create_clipping_imagens_table', 1),
('2015_07_15_141004_create_contato_table', 1),
('2015_07_15_141027_create_contatos_recebidos_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `perfil`
--

CREATE TABLE IF NOT EXISTS `perfil` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `perfil`
--

INSERT INTO `perfil` (`id`, `texto`, `imagem`, `created_at`, `updated_at`) VALUES
(1, '<p><strong>Samara Morelli</strong> nasceu em Alto Alegre, S&atilde;o Paulo, formou-se em arquitetura e urbanismo pela PUC Campinas e &eacute; p&oacute;s graduada em Design de Interiores Contempor&acirc;neo pelo IED - Istituto Europeo di Design. Traz ao mercado suas experi&ecirc;ncias adquiridas em respeitados escrit&oacute;rios de arquitetura e uma grande bagagem multidisciplinar de especializa&ccedil;&otilde;es em arquitetura sustent&aacute;vel, design de interiores, design de mobili&aacute;rio e consultoria e execu&ccedil;&atilde;o de obras.</p>\r\n\r\n<p><span style="color:#9e9380"><span style="font-size:17px"><em>Com esp&iacute;rito jovem, inovador e com muita versatilidade, imprime em seus projetos caracter&iacute;sticas singulares que integram solu&ccedil;&otilde;es criativas e funcionais com bom gosto e sofistica&ccedil;&atilde;o, associando conhecimentos da arquitetura e design de Interiores com a arte e a moda.</em></span></span></p>\r\n\r\n<p>Todos os projetos s&atilde;o desenvolvidos lado a lado ao cliente buscando sempre entender suas necessidades e entregar um trabalho que supere suas expectativas.</p>\r\n', '20150716145632_foto perfil.jpg', '0000-00-00 00:00:00', '2015-07-16 18:35:41');

-- --------------------------------------------------------

--
-- Estrutura da tabela `projetos`
--

CREATE TABLE IF NOT EXISTS `projetos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `projeto_categoria_id` int(11) NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ano` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `local` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `metragem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `projetos_slug_unique` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Extraindo dados da tabela `projetos`
--

INSERT INTO `projetos` (`id`, `projeto_categoria_id`, `ordem`, `slug`, `titulo`, `ano`, `local`, `metragem`, `capa`, `created_at`, `updated_at`) VALUES
(4, 2, 0, 'lorem-ipsum-dolor-sit-amet', 'Lorem ipsum dolor sit amet', '2015', 'Lorem Ipsum', '20m²', '20150720162113_DSC00047.JPG', '2015-07-20 19:21:15', '2015-07-20 19:21:15'),
(5, 2, 1, 'lorem-ipsum-dolor-sit-amet-2', 'Lorem ipsum dolor sit amet 2', '2015', 'Lorem Ipsum', '20m²', '20150720162142_DSC00063.JPG', '2015-07-20 19:21:44', '2015-07-20 19:21:44'),
(6, 2, 2, 'lorem-ipsum-dolor-sit-amet-3', 'Lorem ipsum dolor sit amet 3', '2015', 'Lorem Ipsum', '20m²', '20150720162151_IMG_3536.JPG', '2015-07-20 19:21:52', '2015-07-20 19:21:52'),
(7, 2, 3, 'lorem-ipsum-dolor-sit-amet-4', 'Lorem ipsum dolor sit amet 4', '2015', 'Lorem Ipsum', '20m²', '20150720162215_DSC00047.JPG', '2015-07-20 19:22:00', '2015-07-20 19:22:16');

-- --------------------------------------------------------

--
-- Estrutura da tabela `projetos_categorias`
--

CREATE TABLE IF NOT EXISTS `projetos_categorias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `projetos_categorias_slug_unique` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `projetos_categorias`
--

INSERT INTO `projetos_categorias` (`id`, `ordem`, `slug`, `titulo`, `created_at`, `updated_at`) VALUES
(1, 3, 'mostras', 'mostras', '2015-07-15 22:59:27', '2015-07-17 16:48:38'),
(2, 0, 'arquitetura', 'arquitetura', '2015-07-15 22:59:31', '2015-07-15 22:59:31'),
(3, 1, 'interiores', 'interiores', '2015-07-15 22:59:34', '2015-07-20 16:16:20'),
(4, 2, 'comerciais', 'comerciais', '2015-07-20 16:16:27', '2015-07-20 16:16:27'),
(5, 4, 'concursos', 'concursos', '2015-07-20 16:16:37', '2015-07-20 16:16:37');

-- --------------------------------------------------------

--
-- Estrutura da tabela `projetos_imagens`
--

CREATE TABLE IF NOT EXISTS `projetos_imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `projeto_id` int(11) NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Extraindo dados da tabela `projetos_imagens`
--

INSERT INTO `projetos_imagens` (`id`, `projeto_id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 2, 0, '20150717141721_01.jpg', '2015-07-17 17:17:23', '2015-07-17 17:17:23'),
(2, 2, 2, '20150717141733_IMG_3383.jpg', '2015-07-17 17:17:35', '2015-07-17 17:17:35'),
(3, 2, 1, '20150717141743_IMG_3536.JPG', '2015-07-17 17:17:44', '2015-07-17 17:17:44'),
(4, 4, 2, '20150720162255_01.jpg', '2015-07-20 19:22:57', '2015-07-20 19:22:57'),
(5, 4, 3, '20150720162301_03.jpg', '2015-07-20 19:23:03', '2015-07-20 19:23:03'),
(6, 4, 1, '20150720162307_IMG_3384.jpg', '2015-07-20 19:23:09', '2015-07-20 19:23:09'),
(7, 4, 0, '20150720162314_IMG_3531.JPG', '2015-07-20 19:23:15', '2015-07-20 19:23:15');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuarios_email_unique` (`email`),
  UNIQUE KEY `usuarios_username_unique` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `email`, `username`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'contato@trupe.net', 'trupe', '$2y$10$vjkJktA7ccoJyd9YnghheeEm4YtgIjsCygF0wKU0mUpsFcwR.cLrW', 'hYSDI3GJNsYj62SUTfWdeTXj3frTaKGwNXGNT4Dh1zgtBjyK3aSxpbZuYBMD', '0000-00-00 00:00:00', '2015-07-20 16:20:48');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
