<?php

namespace Painel;

use \ProjetoCategoria, \Input, \Validator, \Redirect, \Session, \Str;

class ProjetosCategoriasController extends BasePainelController {

    private $validation_rules = [
        'titulo' => 'required',
    ];

    public function index()
    {
        $categorias = ProjetoCategoria::ordenados()->get();

        return $this->view('painel.projetos.categorias.index', compact('categorias'));
    }

    public function create()
    {
        return $this->view('painel.projetos.categorias.create');
    }

    public function store()
    {
        $input = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['slug'] = Str::slug(Input::get('titulo'));
            ProjetoCategoria::create($input);
            Session::flash('sucesso', 'Categoria adicionada com sucesso.');

            return Redirect::route('painel.projetos.categorias.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao adicionar categoria. Verifique se já existe outra categoria com o mesmo título.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $categoria = ProjetoCategoria::findOrFail($id);

        return $this->view('painel.projetos.categorias.edit', compact('categoria'));
    }

    public function update($id)
    {
        $categoria = ProjetoCategoria::findOrFail($id);
        $input     = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['slug'] = Str::slug(Input::get('titulo'));
            $categoria->update($input);
            Session::flash('sucesso', 'Categoria alterada com sucesso.');

            return Redirect::route('painel.projetos.categorias.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar categoria. Verifique se já existe outra categoria com o mesmo título.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            ProjetoCategoria::destroy($id);
            Session::flash('sucesso', 'Categoria removida com sucesso.');

            return Redirect::route('painel.projetos.categorias.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover categoria.']);

        }
    }

}
