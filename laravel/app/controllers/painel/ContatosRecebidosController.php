<?php

namespace Painel;

use \ContatoRecebido, \Session, \Redirect;

class ContatosRecebidosController extends BasePainelController {

    public function index()
    {
        $contatos_recebidos = ContatoRecebido::orderBy('id', 'DESC')->paginate(10);

        return $this->view('painel.contatosrecebidos.index', compact('contatos_recebidos'));
    }

    public function show($id)
    {
        $contato = ContatoRecebido::findOrFail($id);
        $contato->update(['lido' => 1]);

        $recebidos_count = ContatoRecebido::naoLidos()->count();
        \View::share(compact('recebidos_count'));

        return $this->view('painel.contatosrecebidos.show', compact('contato'));
    }

    public function destroy($id)
    {
        try {

            ContatoRecebido::destroy($id);
            Session::flash('sucesso', 'Mensagem removida com sucesso.');

            return Redirect::route('painel.contato.recebidos.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover mensagem.']);

        }
    }

}
