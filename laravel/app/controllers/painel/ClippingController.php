<?php

namespace Painel;

use \Clipping, \View, \Input, \Session, \Redirect, \Validator, \Str, \CropImage;

class ClippingController extends BasePainelController {

    private $validation_rules = [
        'data'   => 'required',
        'titulo' => 'required',
        'capa'   => 'required|image'
    ];

    private $image_config = [
        'width'  => 218,
        'height' => 290,
        'path'   => 'assets/img/clipping/capa/'
    ];

    public function index()
    {
        $clipping = Clipping::ordenados()->paginate(10);

        return $this->view('painel.clipping.index', compact('clipping'));
    }

    public function create()
    {
        return $this->view('painel.clipping.create');
    }

    public function store()
    {
        $input = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['slug'] = Str::slug(Input::get('titulo'));
            $input['capa'] = CropImage::make('capa', $this->image_config);
            Clipping::create($input);
            Session::flash('sucesso', 'Projeto criado com sucesso.');

            return Redirect::route('painel.clipping.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao criar projeto. Verifique se já existe outro projeto com o mesmo título.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $clipping = Clipping::findOrFail($id);

        return $this->view('painel.clipping.edit', compact('clipping'));
    }

    public function update($id)
    {
        $clipping = Clipping::findOrFail($id);
        $input    = Input::all();

        $this->validation_rules['capa'] = 'image';
        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['slug'] = Str::slug(Input::get('titulo'));

            if (Input::hasFile('capa')) {
                $input['capa'] = CropImage::make('capa', $this->image_config);
            } else {
                unset($input['capa']);
            }

            $clipping->update($input);
            Session::flash('sucesso', 'Projeto alterado com sucesso.');

            return Redirect::route('painel.clipping.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar projeto. Verifique se já existe outro projeto com o mesmo título.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            Clipping::destroy($id);
            Session::flash('sucesso', 'Projeto removido com sucesso.');

            return Redirect::route('painel.clipping.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover projeto.']);

        }
    }

}
