<?php

namespace Painel;

use \Clipping, \ClippingImagem, \Input, \Session, \Redirect, \Validator, \CropImage;

class ClippingImagensController extends BasePainelController {

    private $validation_rules = [
        'imagem'   => 'required|image'
    ];

    private $image_config = [
        'width'  => 472,
        'height' => 630,
        'path'   => 'assets/img/clipping/'
    ];

    public function index($clipping_id)
    {
        $clipping = Clipping::find($clipping_id);
        if(!$clipping) return Redirect::route('painel.clipping.index');

        $imagens = ClippingImagem::projeto($clipping_id)->ordenados()->get();

        return $this->view('painel.clipping.imagens.index', compact('clipping', 'imagens'));
    }

    public function create($clipping_id)
    {
        $clipping = Clipping::find($clipping_id);
        if(!$clipping) return Redirect::route('painel.clipping.index');

        return $this->view('painel.clipping.imagens.create', compact('clipping'));
    }

    public function store($clipping_id)
    {
        $input = Input::all();
        $input['clipping_id'] = $clipping_id;

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['imagem'] = CropImage::make('imagem', $this->image_config);
            ClippingImagem::create($input);
            Session::flash('sucesso', 'Imagem inserida com sucesso.');

            return Redirect::route('painel.clipping.imagens.index', $input['clipping_id']);

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao inserir imagem.'])
                ->withInput();

        }
    }

    public function destroy($clipping_id, $imagem_id)
    {
        try {

            ClippingImagem::destroy($imagem_id);
            Session::flash('sucesso', 'Imagem removida com sucesso.');

            return Redirect::route('painel.clipping.imagens.index', $clipping_id);

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover imagem.']);

        }
    }

}
