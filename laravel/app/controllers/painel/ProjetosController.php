<?php

namespace Painel;

use \Projeto, \ProjetoCategoria, \Input, \Validator, \Redirect, \Session, \Str, \CropImage;

class ProjetosController extends BasePainelController {

    private $validation_rules = [
        'projeto_categoria_id' => 'required',
        'capa'                 => 'required|image',
        'titulo'               => 'required',
        'ano'                  => 'required',
        'local'                => 'required',
        'metragem'             => 'required',
    ];

    private $image_config = [
        'width'  => 475,
        'height' => 160,
        'path'   => 'assets/img/projetos/capa/'
    ];

    public function index()
    {
        $categorias = ProjetoCategoria::lists('titulo', 'id');

        if (Input::has('filtro') && ProjetoCategoria::find(Input::get('filtro'))) {
            $filtro   = Input::get('filtro');
            $projetos = Projeto::ordenados()->categoria($filtro)->get();
        } else {
            $filtro   = null;
            $projetos = Projeto::orderBy('projeto_categoria_id', 'ASC')->ordenados()->get();
        }

        return $this->view('painel.projetos.index', compact('projetos', 'categorias', 'filtro'));
    }

    public function create()
    {
        $categorias = ProjetoCategoria::lists('titulo', 'id');

        return $this->view('painel.projetos.create', compact('categorias'));
    }

    public function store()
    {
        $input = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['slug'] = Str::slug(Input::get('titulo'));
            $input['capa'] = CropImage::make('capa', $this->image_config);

            Projeto::create($input);
            Session::flash('sucesso', 'Projeto adicionado com sucesso.');

            return Redirect::route('painel.projetos.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao criar projeto. Verifique se já existe outro projeto com o mesmo título.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $projeto    = Projeto::findOrFail($id);
        $categorias = ProjetoCategoria::lists('titulo', 'id');

        return $this->view('painel.projetos.edit', compact('projeto', 'categorias'));
    }

    public function update($id)
    {
        $projeto = Projeto::findOrFail($id);
        $input   = Input::all();

        $this->validation_rules['capa'] = 'image';
        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['slug'] = Str::slug(Input::get('titulo'));

            if (Input::hasFile('capa')) {
                $input['capa'] = CropImage::make('capa', $this->image_config);
            } else {
                unset($input['capa']);
            }

            $projeto->update($input);
            Session::flash('sucesso', 'Projeto alterado com sucesso.');

            return Redirect::route('painel.projetos.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar projeto. Verifique se já existe outro projeto com o mesmo título.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            Projeto::destroy($id);
            Session::flash('sucesso', 'Projeto removido com sucesso.');

            return Redirect::route('painel.projetos.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover projeto.']);

        }
    }

}
