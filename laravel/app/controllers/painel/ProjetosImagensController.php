<?php

namespace Painel;

use \Projeto, \ProjetoImagem, \Input, \Session, \Redirect, \Validator, \CropImage;

class ProjetosImagensController extends BasePainelController {

    private $validation_rules = [
        'imagem'   => 'required|image'
    ];

    private $image_config = [
        'width'  => 800,
        'height' => null,
        'path'   => 'assets/img/projetos/'
    ];

    public function index($projeto_id)
    {
        $projeto = Projeto::find($projeto_id);
        if(!$projeto) return Redirect::route('painel.projetos.index');

        $imagens = ProjetoImagem::projeto($projeto_id)->ordenados()->get();

        return $this->view('painel.projetos.imagens.index', compact('projeto', 'imagens'));
    }

    public function create($projeto_id)
    {
        $projeto = Projeto::find($projeto_id);
        if(!$projeto) return Redirect::route('painel.projetos.index');

        return $this->view('painel.projetos.imagens.create', compact('projeto'));
    }

    public function store($projeto_id)
    {
        $input = Input::all();
        $input['projeto_id'] = $projeto_id;

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['imagem'] = CropImage::make('imagem', $this->image_config);
            ProjetoImagem::create($input);
            Session::flash('sucesso', 'Imagem inserida com sucesso.');

            return Redirect::route('painel.projetos.imagens.index', $input['projeto_id']);

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao inserir imagem.'])
                ->withInput();

        }
    }

    public function destroy($projeto_id, $imagem_id)
    {
        try {

            ProjetoImagem::destroy($imagem_id);
            Session::flash('sucesso', 'Imagem removida com sucesso.');

            return Redirect::route('painel.projetos.imagens.index', $projeto_id);

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover imagem.']);

        }
    }

}
