<?php

use \Projeto;

class ProjetosController extends BaseController {

	public function index($categoria_slug = null)
	{
        $categorias = ProjetoCategoria::ordenados()->get();

        if (!$categoria_slug) {
            $categoria_selecionada = $categorias->first();
        } else {
            $categoria_selecionada = ProjetoCategoria::slug($categoria_slug)->first();
        }

        if (!$categoria_selecionada) App::abort('404');

        $projetos = Projeto::categoria($categoria_selecionada->id)->ordenados()->get();

        View::share(compact('categorias', 'categoria_selecionada'));
		return $this->view('frontend.projetos.index', compact('projetos'));
	}

    public function show($categoria_slug = null, $projeto_slug = null)
    {
        $categorias = ProjetoCategoria::ordenados()->get();

        $categoria_selecionada = ProjetoCategoria::slug($categoria_slug)->first();
        if (!$categoria_selecionada) App::abort('404');

        $projeto = Projeto::categoria($categoria_selecionada->id)->with('imagens')->slug($projeto_slug)->first();
        if (!$projeto) App::abort('404');

        $ordem_projeto = $projeto->ordem;
        $anterior = Projeto::select('slug')
                        ->where('ordem', '<', $ordem_projeto)
                        ->orWhere(function($query) use ($ordem_projeto, $projeto) {
                          $query->where('ordem', '=', $ordem_projeto)
                                ->where('id', '<', $projeto->id);
                        })
                        ->categoria($categoria_selecionada->id)
                        ->orderBy('ordem', 'DESC')
                        ->orderBy('id', 'DESC')
                        ->first();
        $proximo  = Projeto::select('slug')
                        ->where('ordem', '>', $ordem_projeto)
                        ->orWhere(function($query) use ($ordem_projeto, $projeto) {
                          $query->where('ordem', '=', $ordem_projeto)
                                ->where('id', '>', $projeto->id);
                        })
                        ->categoria($categoria_selecionada->id)
                        ->orderBy('ordem', 'ASC')
                        ->orderBy('id', 'ASC')
                        ->first();

        View::share(compact('categorias', 'categoria_selecionada'));
        return $this->view('frontend.projetos.show', compact('projeto', 'anterior', 'proximo'));
    }

}
