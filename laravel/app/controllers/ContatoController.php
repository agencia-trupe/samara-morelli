<?php

class ContatoController extends BaseController {

    public function index()
    {
        return $this->view('frontend.contato');
    }

    public function envio()
    {
        $contato = Contato::first();

        $nome = Input::get('nome');
        $email = Input::get('email');
        $telefone = Input::get('telefone');
        $mensagem = Input::get('mensagem');

        $validation = Validator::make(
            array(
                'nome'     => $nome,
                'email'    => $email,
                'mensagem' => $mensagem
            ),
            array(
                'nome'     => 'required',
                'email'    => 'required|email',
                'mensagem' => 'required'
            )
        );

        if ($validation->fails())
        {
            $response = array(
                'status'  => 'fail'
            );
            return Response::json($response);
        }

        if (isset($contato->email))
        {
            $data = array(
                'nome'     => $nome,
                'email'    => $email,
                'telefone' => $telefone,
                'mensagem' => $mensagem
            );

            Mail::send('emails.contato', $data, function($message) use ($data, $contato)
            {
                $message->to($contato->email, Config::get('projeto.name'))
                        ->subject('[CONTATO] '.Config::get('projeto.name'))
                        ->replyTo($data['email'], $data['nome']);
            });
        }

        $object = new ContatoRecebido;
        $object->nome = $nome;
        $object->email = $email;
        $object->telefone = $telefone;
        $object->mensagem = $mensagem;
        $object->save();

        $response = array(
            'status'  => 'success'
        );
        return Response::json($response);
    }

}
