<?php

use \Clipping;

class ClippingController extends BaseController {

	public function index()
	{
        $clipping = Clipping::ordenados()->get();

		return $this->view('frontend.clipping.index', compact('clipping'));
	}

    public function show($slug)
    {
        $clipping = Clipping::with('imagens')->slug($slug)->first();
        if (!$clipping) App::abort('404');

        $data_clipping = Carbon\Carbon::createFromFormat('m/Y', $clipping->data)->format('Y-m');
        $anterior  = Clipping::select('slug')
                        ->where('data', '>', $data_clipping)
                        ->orWhere(function($query) use ($data_clipping, $clipping) {
                          $query->where('data', $data_clipping)
                                ->where('id', '>', $clipping->id);
                        })
                        ->orderBy('data', 'ASC')
                        ->orderBy('id', 'ASC')
                        ->first();
        $proximo = Clipping::select('slug')
                        ->where('data', '<', $data_clipping)
                        ->orWhere(function($query) use ($data_clipping, $clipping) {
                          $query->where('data', $data_clipping)
                                ->where('id', '<', $clipping->id);
                        })
                        ->orderBy('data', 'DESC')
                        ->orderBy('id', 'DESC')
                        ->first();

        return $this->view('frontend.clipping.show', compact('clipping', 'anterior', 'proximo'));
    }

}
