<?php

// Front-end

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

Route::get('perfil', ['as' => 'perfil', 'uses' => 'PerfilController@index']);

Route::get('projetos/{categoria_slug?}', [
    'as'   => 'projetos',
    'uses' => 'ProjetosController@index'
]);
Route::get('projetos/{categoria_slug}/{projeto_slug}', [
    'as'   => 'projetos.show',
    'uses' => 'ProjetosController@show'
]);

Route::get('clipping', [
    'as'   => 'clipping',
    'uses' => 'ClippingController@index'
]);
Route::get('clipping/{projeto_slug}', [
    'as'   => 'clipping.show',
    'uses' => 'ClippingController@show'
]);

Route::get('contato', [
    'as'   => 'contato',
    'uses' => 'ContatoController@index'
]);
Route::post('contato', [
    'as'   => 'contato.envio',
    'uses' => 'ContatoController@envio'
]);


// Painel

Route::get('painel', [
    'before' => 'auth',
    'as'     => 'painel.home',
    'uses'   => 'Painel\HomeController@index'
]);

Route::get('painel/login', [
    'as'   => 'painel.login',
    'uses' => 'Painel\HomeController@login'
]);
Route::post('painel/login', [
    'as'   => 'painel.auth',
    'uses' => 'Painel\HomeController@attempt'
]);
Route::get('painel/logout', [
    'as'   => 'painel.logout',
    'uses' => 'Painel\HomeController@logout'
]);

Route::group(['prefix' => 'painel', 'before' => 'auth'], function() {
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

    Route::resource('banners', 'Painel\BannersController');

    Route::resource('perfil', 'Painel\PerfilController');

    Route::resource('projetos/categorias', 'Painel\ProjetosCategoriasController');
    Route::resource('projetos', 'Painel\ProjetosController');
    Route::resource('projetos.imagens', 'Painel\ProjetosImagensController');

    Route::resource('clipping', 'Painel\ClippingController');
    Route::resource('clipping.imagens', 'Painel\ClippingImagensController');

    Route::resource('contato/recebidos', 'Painel\ContatosRecebidosController');
    Route::resource('contato', 'Painel\ContatoController');

    Route::resource('usuarios', 'Painel\UsuariosController');

    Route::post('ajax/order', 'Painel\AjaxController@order');
});
