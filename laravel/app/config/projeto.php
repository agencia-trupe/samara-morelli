<?php

return array(

    'name'        => 'Samara Morelli',
    'title'       => 'Samara Morelli',
    'description' => 'SAMARA MORELLI | arquitetura . interiores - especializações em arquitetura, consultoria e execução de obras, urbanismo, sustentabilidade, design de interiores, design de mobiliário.',
    'keywords'    => 'arquitetura,arquiteto, arquiteta, projeto, projetos, projetos de arquitetura, projetar, design de interiores, design, decoração, decoração de interiores, interiores, decoradora, decorador, decora, construção, projeto residencial, projeto comercial, projeto de interiores, acompanhamento de obra',
    'share_image' => 'samaramorelli.png'

);
