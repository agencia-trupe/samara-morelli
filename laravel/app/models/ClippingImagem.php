<?php

class ClippingImagem extends Eloquent
{

    protected $table = 'clipping_imagens';

    protected $hidden = [];

    protected $guarded = ['id'];

    public function scopeProjeto($query, $id)
    {
        return $query->where('clipping_id', $id);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

}
