<?php

class ProjetoCategoria extends Eloquent
{

    protected $table = 'projetos_categorias';

    protected $hidden = [];

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeSlug($query, $slug)
    {
        return $query->whereSlug($slug);
    }

    public function projetos()
    {
        return $this->hasMany('Projeto', 'projeto_categoria_id');
    }

}
