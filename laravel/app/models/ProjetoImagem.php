<?php

class ProjetoImagem extends Eloquent
{

    protected $table = 'projetos_imagens';

    protected $hidden = [];

    protected $guarded = ['id'];

    public function scopeProjeto($query, $id)
    {
        return $query->where('projeto_id', $id);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

}
