<?php

class Projeto extends Eloquent
{

    protected $table = 'projetos';

    protected $hidden = [];

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeSlug($query, $slug)
    {
        return $query->whereSlug($slug);
    }

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->where('projeto_categoria_id', $categoria_id);
    }

    public function categoriaParent()
    {
        return $this->belongsTo('ProjetoCategoria', 'projeto_categoria_id');
    }

    public function imagens()
    {
        return $this->hasMany('ProjetoImagem', 'projeto_id')->orderBy('ordem', 'ASC');
    }

}
