<?php

class ContatoSeeder extends Seeder {

    public function run()
    {
        DB::table('contato')->delete();

        $data = array(
            array(
                'ddd'        => '19',
                'telefone'   => '98431.0403',
                'email'      => 'contato@samaramorelli.com.br',
                'endereco'   => '...',
                'googlemaps' => '...',
            )
        );

        DB::table('contato')->insert($data);
    }

}
