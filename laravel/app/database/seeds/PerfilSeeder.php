<?php

class PerfilSeeder extends Seeder {

    public function run()
    {
        DB::table('perfil')->delete();

        $data = array(
            array(
                'texto'  => '...',
                'imagem' => 'placeholder',
            )
        );

        DB::table('perfil')->insert($data);
    }

}
