@section('content')

    <legend>
        <h2><small>Projetos /</small> Editar Categoria</h2>
    </legend>

    {{ Form::model($categoria, [
        'route' => ['painel.projetos.categorias.update', $categoria->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.projetos.categorias._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop
