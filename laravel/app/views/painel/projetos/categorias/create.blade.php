@section('content')

    <legend>
        <h2><small>Projetos /</small> Adicionar Categoria</h2>
    </legend>

    {{ Form::open(['route' => 'painel.projetos.categorias.store']) }}

        @include('painel.projetos.categorias._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
