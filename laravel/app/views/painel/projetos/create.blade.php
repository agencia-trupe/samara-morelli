@section('content')

    <legend>
        <h2><small>Projetos /</small> Adicionar Projeto</h2>
    </legend>

    {{ Form::open(['route' => 'painel.projetos.store', 'files' => true]) }}

        @include('painel.projetos._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
