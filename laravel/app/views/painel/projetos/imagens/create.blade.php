@section('content')

    <legend>
        <h2><small>Projetos / Adicionar Imagem ao Projeto:</small> {{ $projeto->titulo }}</h2>
    </legend>

    {{ Form::open(['route' => ['painel.projetos.imagens.store', $projeto->id], 'files' => true]) }}

        @include('painel.projetos.imagens._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
