@section('content')

    @if($errors->any())
        @foreach($errors->all() as $error)
        <div class="alert alert-block alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ $error }}
        </div>
        @endforeach
    @endif

    @if(Session::has('sucesso'))
       <div class="alert alert-block alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ Session::get('sucesso') }}
        </div>
    @endif

    <legend>
        <h2>
            Projetos
            <div class="btn-group pull-right">
                <a href="{{ route('painel.projetos.categorias.index') }}" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-edit" style="margin-right:10px;"></span>Editar Categorias</a>
                <a href="{{ route('painel.projetos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Projeto</a>
            </div>
        </h2>
    </legend>

    <div class="row">
        <div class="form-group col-sm-4">
            {{ Form::select('filtro', ['' => 'Todas as Categorias'] + $categorias, Input::get('filtro'), ['class' => 'form-control', 'id' => 'filtro-select', 'style' => 'height:49px;']) }}
        </div>
        @if(!$filtro)
        <div class="col-sm-4">
            <p class="alert alert-info small" style="margin-bottom: 15px;">
                <span class="glyphicon glyphicon-info-sign" style="margin-right:10px;"></span>
                Selecione uma categoria para ordenar os projetos.
            </p>
        </div>
        @endif
    </div>

    @if(count($projetos))
    <table class="table table-striped table-bordered table-hover table-sortable" data-tabela="projetos">
        <thead>
            <tr>
                @if($filtro)<th>Ordenar</th>@endif
                @if(!$filtro)<th>Categoria</th>@endif
                <th>Ano</th>
                <th>Capa</th>
                <th>Título</th>
                <th>Imagens</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($projetos as $projeto)

            <tr class="tr-row" id="id_{{ $projeto->id }}">
                @if($filtro)<td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>@endif
                @if(!$filtro)<td>@if($projeto->categoriaParent){{ $projeto->categoriaParent->titulo }}@endif</td>@endif
                <td>{{ $projeto->ano }}</td>
                <td><img src="{{ url('assets/img/projetos/capa/'.$projeto->capa) }}" alt="" style="width:100%;max-width:150px;height:auto;"></td>
                <td>{{ $projeto->titulo }}</td>
                <td><a href="{{ route('painel.projetos.imagens.index', $projeto->id) }}" class="btn btn-default btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Gerenciar
                </a></td>
                <td class="crud-actions">
                    {{ Form::open(array('route' => array('painel.projetos.destroy', $projeto->id), 'method' => 'delete')) }}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.projetos.edit', $projeto->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {{ Form::close() }}
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
    @else
        <div class="alert alert-warning" role="alert">Nenhum projeto cadastrado.</div>
    @endif

@stop
