@if($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ $error }}
    </div>
    @endforeach
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="form-group">
    {{ Form::label('projeto_categoria_id', 'Categoria') }}
    {{ Form::select('projeto_categoria_id', ['' => 'Selecione'] + $categorias, null, ['class' => 'form-control']) }}
</div>

<div class="well form-group">
    {{ Form::label('capa', 'Imagem de capa') }}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/projetos/capa/'.$projeto->capa) }}" style="display:block; margin-bottom: 10px; max-width:300px;height:auto;">
@endif
    {{ Form::file('capa', ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('titulo', 'Título') }}
    {{ Form::text('titulo', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('ano', 'Ano') }}
    {{ Form::text('ano', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('local', 'Local') }}
    {{ Form::text('local', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('metragem', 'Metragem') }}
    {{ Form::text('metragem', null, ['class' => 'form-control']) }}
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}

<a href="{{ route('painel.projetos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
