@section('content')

    <legend>
        <h2><small>Clipping / Adicionar Imagem ao Projeto:</small> {{ $clipping->titulo }}</h2>
    </legend>

    {{ Form::open(['route' => ['painel.clipping.imagens.store', $clipping->id], 'files' => true]) }}

        @include('painel.clipping.imagens._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
