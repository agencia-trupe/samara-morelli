@section('content')

    @if($errors->any())
        @foreach($errors->all() as $error)
        <div class="alert alert-block alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ $error }}
        </div>
        @endforeach
    @endif

    @if(Session::has('sucesso'))
       <div class="alert alert-block alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ Session::get('sucesso') }}
        </div>
    @endif

    <legend>
        <h2>
            Clipping
            <a href="{{ route('painel.clipping.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Projeto</a>
        </h2>
    </legend>

    @if(count($clipping))
    <table class="table table-striped table-bordered table-hover table-sortable" data-tabela="clipping">
        <thead>
            <tr>
                <th>Data</th>
                <th>Capa</th>
                <th>Título</th>
                <th>Imagens</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($clipping as $clipping_projeto)

            <tr class="tr-row" id="id_{{ $clipping_projeto->id }}">
                <td>{{ $clipping_projeto->data }}</td>
                <td><img src="{{ url('assets/img/clipping/capa/'.$clipping_projeto->capa) }}" alt="" style="width:100%;max-width:150px;height:auto;"></td>
                <td>{{ $clipping_projeto->titulo }}</td>
                <td><a href="{{ route('painel.clipping.imagens.index', $clipping_projeto->id) }}" class="btn btn-default btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Gerenciar
                </a></td>
                <td class="crud-actions">
                    {{ Form::open(array('route' => array('painel.clipping.destroy', $clipping_projeto->id), 'method' => 'delete')) }}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.clipping.edit', $clipping_projeto->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {{ Form::close() }}
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
    {{ $clipping->links() }}
    @else
        <div class="alert alert-warning" role="alert">Nenhum projeto cadastrado.</div>
    @endif

@stop
