@section('content')

    <legend>
        <h2><small>Clipping /</small> Editar Projeto</h2>
    </legend>

    {{ Form::model($clipping, [
        'route' => ['painel.clipping.update', $clipping->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.clipping._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop
