@section('content')

    <legend>
        <h2><small>Clipping /</small> Adicionar Projeto</h2>
    </legend>

    {{ Form::open(['route' => 'painel.clipping.store', 'files' => true]) }}

        @include('painel.clipping._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
