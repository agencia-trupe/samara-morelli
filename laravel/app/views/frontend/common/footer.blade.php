    <footer>
        <div class="center">
            <p>
                <span>© {{ date('Y') }} {{ Config::get('projeto.name') }} · Todos os direitos reservados ·</span>
                <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:
                <a href="http://www.trupe.net" target="_blank" class="trupe">Trupe Agência Criativa</a>
            </p>
        </div>
    </footer>
