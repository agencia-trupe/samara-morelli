    <header>
        <div class="center">
            <h1><a href="{{ route('home') }}">{{ Config::get('projeto.title') }}</a></h1>
            <ul id="nav-desktop">
                <li>
                    <a href="{{ route('perfil') }}" @if(str_is('perfil*', Route::currentRouteName())) class='active' @endif>Perfil</a>
                </li>
                <li>
                    <a href="{{ route('projetos') }}" @if(str_is('projetos*', Route::currentRouteName())) class='active' @endif>Projetos</a>
                    @if(str_is('projetos*', Route::currentRouteName()) && count($categorias))
                    <ul>
                    @foreach($categorias as $categoria)
                        <li><a href="{{ route('projetos', $categoria->slug) }}" @if($categoria->id == $categoria_selecionada->id) class="active" @endif>{{ $categoria->titulo }}</a></li>
                    @endforeach
                    </ul>
                    @endif
                </li>
                <li>
                    <a href="{{ route('clipping') }}" @if(str_is('clipping*', Route::currentRouteName())) class='active' @endif>Clipping</a>
                </li>
                <li>
                    <a href="http://samaramorelli.wordpress.com">Blog</a>
                </li>
                <li>
                    <a href="{{ route('contato') }}" @if(str_is('contato*', Route::currentRouteName())) class='active' @endif>Contato</a>
                </li>
                <li class="social">
                    @if($contato->facebook)
                    <a href="{{ $contato->facebook }}" class="facebook" target="_blank">facebook</a>
                    @endif
                    @if($contato->instagram)
                    <a href="{{ $contato->instagram }}" class="instagram" target="_blank">instagram</a>
                    @endif
                </li>
            </ul>
        </div>

        <nav id="nav-mobile">
            <a href="{{ route('perfil') }}" @if(str_is('perfil*', Route::currentRouteName())) class='active' @endif>Perfil</a>
            <a href="{{ route('projetos') }}" @if(str_is('projetos*', Route::currentRouteName())) class='active' @endif>Projetos</a>
            <a href="{{ route('clipping') }}" @if(str_is('clipping*', Route::currentRouteName())) class='active' @endif>Clipping</a>
            <a href="#" @if(str_is('blog*', Route::currentRouteName())) class='active' @endif>Blog</a>
            <a href="{{ route('contato') }}" @if(str_is('contato*', Route::currentRouteName())) class='active' @endif>Contato</a>
            <div class="social">
                @if($contato->facebook)
                <a href="{{ $contato->facebook }}" class="facebook" target="_blank">facebook</a>
                @endif
                @if($contato->instagram)
                <a href="{{ $contato->instagram }}" class="instagram" target="_blank">instagram</a>
                @endif
            </div>
        </nav>
        <button id="mobile-toggle" type="button" role="button">
            <span class="lines"></span>
        </button>

        @if(str_is('projetos*', Route::currentRouteName()) && count($categorias))
        <div class="categorias-bg">
            <div class="center">
                <ul>
                @foreach($categorias as $categoria)
                    <li><a href="{{ route('projetos', $categoria->slug) }}" @if($categoria->id == $categoria_selecionada->id) class="active" @endif>{{ $categoria->titulo }}</a></li>
                @endforeach
                </ul>
            </div>
        </div>
        @endif
    </header>
