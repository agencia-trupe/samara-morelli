@section('content')

    <div class="main" id="contato">
        <div class="googlemaps">{{ $contato->googlemaps }}</div>

        <div class="center">
            <div class="info">
                <h3>Contato</h3>
                <p class="telefone">
                    <span>{{ $contato->ddd }}</span>
                    {{ $contato->telefone }}
                </p>
                <div class="endereco">
                    {{ $contato->endereco }}
                </div>
                <a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a>
            </div>

            <div class="fale-conosco">
                <h3>Fale conosco</h3>
                <form action="" id="form-contato">
                    <div class="left">
                        <input type="text" name="nome" id="nome" placeholder="Nome" required>
                        <input type="email" name="email" id="email" placeholder="E-mail" required>
                        <input type="text" name="telefone" id="telefone" placeholder="Telefone">
                    </div>
                    <textarea name="mensagem" id="mensagem" placeholder="Mensagem" required></textarea>
                    <div id="erro">Preencha todos os campos corretamente.</div>
                    <input type="submit" value="Enviar">
                </form>
                <div id="sucesso">
                    Mensagem enviada com sucesso
                    <span>Em breve entraremos em contato</span>
                </div>
            </div>
        </div>
    </div>

@stop
