@section('content')

    <div id="banners-home">
        @foreach($banners as $banner)
        <div class="slide" style="background-image: url('{{ asset('assets/img/banners/'.$banner->imagem) }}')"></div>
        @endforeach
    </div>

@stop
