@section('content')

    <div class="main" id="perfil">
        <div class="center">
            <div class="imagem">
                <img src="{{ asset('assets/img/perfil/'.$perfil->imagem) }}" alt="">
            </div>

            <div class="texto">
                {{ $perfil->texto }}
            </div>
        </div>
    </div>

@stop
