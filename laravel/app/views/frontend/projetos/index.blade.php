@section('content')

    <div class="main" id="projetos">
        <div class="center">
            <div class="projetos-thumbs">
                @foreach($projetos as $projeto)
                <a href="{{ route('projetos.show', [$categoria_selecionada->slug, $projeto->slug]) }}">
                    <img src="{{ asset('assets/img/projetos/capa/'.$projeto->capa) }}" alt="">
                    <div class="overlay">
                        <div class="overlay-text">
                            {{ $projeto->titulo }}
                            <span>{{ $projeto->ano }}</span>
                        </div>
                    </div>
                </a>
                @endforeach
            </div>
        </div>
    </div>

@stop
