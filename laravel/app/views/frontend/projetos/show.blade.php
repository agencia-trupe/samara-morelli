@section('content')

    <div class="main" id="projetos">
        <div class="center">
            <div class="projetos-show">
                <div class="projetos-info">
                    <h3>{{ $projeto->titulo }}</h3>
                    <p><span>Ano</span> {{ $projeto->ano }}</p>
                    <p><span>Local</span> {{ $projeto->local }}</p>
                    <p><span>Metragem</span> {{ $projeto->metragem }}</p>

                    <div class="projetos-navegacao">
                        @if($anterior)
                        <a href="{{ route('projetos.show', [$categoria_selecionada->slug, $anterior->slug]) }}" class="prev">
                            <span>anterior</span>
                        </a>
                        @else
                        <a class="prev disabled"><span>anterior</span></a>
                        @endif
                        @if($proximo)
                        <a href="{{ route('projetos.show', [$categoria_selecionada->slug, $proximo->slug]) }}" class="next">
                            <span>próximo</span>
                        </a>
                        @else
                        <a class="next disabled"><span>próximo</span></a>
                        @endif
                        <a href="{{ route('projetos', $categoria_selecionada->slug) }}" class="all">
                            <span>todos</span>
                        </a>
                    </div>
                </div>

                <div class="imagens">
                    @foreach($projeto->imagens as $imagem)
                    <img src="{{ asset('assets/img/projetos/'.$imagem->imagem) }}" alt="">
                    @endforeach
                </div>
                <div class="projetos-navegacao mobile">
                    @if($anterior)
                    <a href="{{ route('projetos.show', [$categoria_selecionada->slug, $anterior->slug]) }}" class="prev">
                        <span>anterior</span>
                    </a>
                    @else
                    <a class="prev disabled"><span>anterior</span></a>
                    @endif
                    @if($proximo)
                    <a href="{{ route('projetos.show', [$categoria_selecionada->slug, $proximo->slug]) }}" class="next">
                        <span>próximo</span>
                    </a>
                    @else
                    <a class="next disabled"><span>próximo</span></a>
                    @endif
                    <a href="{{ route('projetos', $categoria_selecionada->slug) }}" class="all">
                        <span>todos</span>
                    </a>
                </div>
            </div>
        </div>
    </div>

@stop
