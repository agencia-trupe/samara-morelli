@section('content')

    <div class="main" id="clipping">
        <div class="center">
            <div class="clipping-show">
                @foreach($clipping->imagens as $imagem)
                <img src="{{ asset('assets/img/clipping/'.$imagem->imagem) }}" alt="">
                @endforeach
            </div>

            <div class="clipping-navegacao">
                @if($anterior)
                <a href="{{ route('clipping.show', $anterior->slug) }}" class="prev">
                    <span>anterior</span>
                </a>
                @else
                <a class="prev disabled"><span>anterior</span></a>
                @endif
                @if($proximo)
                <a href="{{ route('clipping.show', $proximo->slug) }}" class="next">
                    <span>próximo</span>
                </a>
                @else
                <a class="next disabled"><span>próximo</span></a>
                @endif
                <a href="{{ route('clipping') }}" class="all">
                    <span>todos</span>
                </a>
            </div>
        </div>
    </div>

@stop
