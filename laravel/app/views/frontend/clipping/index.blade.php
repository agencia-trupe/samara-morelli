@section('content')

    <div class="main" id="clipping">
        <div class="center">
            <div class="clipping-thumbs">
                @foreach($clipping as $projeto)
                <a href="{{ route('clipping.show', $projeto->slug) }}">
                    <img src="{{ asset('assets/img/clipping/capa/'.$projeto->capa) }}" alt="">
                    <div class="titulo">{{ $projeto->titulo }}</div>
                    <div class="vermais"><span>ver mais</span></div>
                </a>
                @endforeach
            </div>
        </div>
    </div>

@stop
