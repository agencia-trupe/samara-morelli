$('document').ready( function(){

    if ($('#monthpicker').length) {
        $.datepicker.regional['pt-BR'] = {
            changeMonth: true,
            changeYear: true,
            yearRange: '1990:2040',
            showButtonPanel: true,
            onClose: function(dateText, inst) {
                function isDonePressed(){
                    return ($('#ui-datepicker-div').html().indexOf('ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all ui-state-hover') > -1);
                }
                if (isDonePressed()){
                    var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                    var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                    $(this).datepicker('setDate', new Date(year, month, 1)).trigger('change');
                    $('.date-picker').focusout();
                }
            },
            beforeShow : function(input, inst) {
                inst.dpDiv.addClass('month_year_datepicker')

                if ((datestr = $(this).val()).length > 0) {
                    year = datestr.substring(datestr.length-4, datestr.length);
                    month = datestr.substring(0, 2);
                    $(this).datepicker('option', 'defaultDate', new Date(year, month-1, 1));
                    $(this).datepicker('setDate', new Date(year, month-1, 1));
                    $(".ui-datepicker-calendar").hide();
                }
            },
            closeText: 'Aplicar',
            prevText: '&#x3c;Anterior',
            nextText: 'Pr&oacute;ximo&#x3e;',
            currentText: 'Hoje',
            monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
            'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
            monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
            'Jul','Ago','Set','Out','Nov','Dez'],
            dateFormat: 'mm/yy',
        };

        $.datepicker.setDefaults($.datepicker.regional['pt-BR']);

        $("#monthpicker").datepicker();
        $('head').append('<style>.ui-datepicker-calendar{display: none;}.ui-datepicker-title{color:black;font-weight:normal;}</style>');

        if ($('#monthpicker').val() == '') {
            $('#monthpicker').datepicker("setDate", new Date());
        }

    }

  	$('.btn-delete').click( function(e){
    	e.preventDefault();
    	var form = $(this).closest('form');
        bootbox.confirm({
            size: 'small',
            backdrop: true,
            message: 'Deseja excluir o registro?',
            buttons: {
                'cancel': {
                    label: 'Cancelar',
                    className: 'btn-default btn-sm'
                },
                'confirm': {
                    label: '<span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir',
                    className: 'btn-danger btn-sm'
                }
            },
            callback: function(result) {
                if (result) form.submit();
            }
        });
  	});

    $("table.table-sortable tbody").sortable({
        update : function () {
            serial = [];
            tabela = $('table.table-sortable').attr('data-tabela');
            $('table.table-sortable tbody').children('tr').each(function(idx, elm) {
                serial.push(elm.id.split('_')[1])
            });
            $.post(BASE + '/painel/ajax/order', { data : serial , tabela : tabela });
        },
        helper: function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        },
        handle : $('.btn-move')
    }).disableSelection();

    var TEXTAREA_CONFIG = {
        perfil: {
            colorButton_colors: '9e9380',
            colorButton_enableMore: false,
            fontSize_defaultLabel: '14 (padrão)',
            fontSize_sizes: '14 (padrão)/14px;15/15px;16/16px;17/17px;18/18px;19/19px;20/20px;21/21px;22/22px;23/23px;24/24px;',
            toolbar: [['FontSize', 'Bold', 'Italic'], ['TextColor']]
        },

        clean: {
            toolbar: []
        }
    };

    $('.ckeditor').each(function (i, obj) {
        CKEDITOR.replace(obj.id, TEXTAREA_CONFIG[obj.dataset.editor]);
    });

    $('#filtro-select').on('change', function () {
        var id = $(this).val();
        if (id) {
            window.location = BASE + '/painel/projetos?filtro=' + id;
        } else {
            window.location = BASE + '/painel/projetos';
        }
    });

});
